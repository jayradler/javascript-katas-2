function add(num1,num2) {
    return num1+num2;
}

function multiply(num1,num2) {
    let answer = 0;
    for (let i = 1; i <= num2; i++) {
        answer = add(answer,num1);
    }
    return answer;
}

function power(num1,num2) {
    let answer = 1;
    for (let i = 1; i <= num2; i++) {
        answer = multiply(answer,num1);
    }
    return answer;
}

function factorial(num) {
    let answer = 1;
    for (let i = 1; i <= num; i++) {
        answer = multiply(answer,i);
    }
    return answer;
}

function fibonacci(num) {
    let x = 0;
    let y = 1;
    let fib = 1;
    for (let i = 3; i <= num; i++) {
        fib = add(x,y);
        x = y;
        y = fib;
    }
    return fib;
}

console.log(add(2,4))
console.log(multiply(6,8))
console.log(power(2,8))
console.log(factorial(4))
console.log(fibonacci(8))